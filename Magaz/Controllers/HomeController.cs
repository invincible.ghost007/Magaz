﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Magaz.Models;
using Magaz.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.ExpressionTranslators.Internal;

namespace Magaz.Controllers
{

    public class HomeController : Controller
    {
        private readonly ApplicationContext db;
        private readonly UserManager<User> _userManager;

        public HomeController(ApplicationContext context, UserManager<User> userManager)
        {

            this.db = context;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Goods");
            }
            else
            {
                return View();
            }
        }

        public IActionResult Goods()
        {
            BooksViewModel ivm = new BooksViewModel { Books = db.Book.Include(c => c.Author) };
            return View(ivm);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Book books)
        {
            db.Book.Add(books);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    
        public async Task<IActionResult> Details(int? id)
        {
            if (id != null)
            {
                Book book = await db.Book.FirstOrDefaultAsync(p => p.Id == id);
                if (book != null)
                    return View(book);
            }
            return NotFound();
        }
        public async Task<IActionResult> Edit(int? id)
        {
            if (id != null)
            {
                Book book = await db.Book.FirstOrDefaultAsync(p => p.Id == id);
                if (book != null)
                    return View(book);
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Book book)
        {
            db.Book.Update(book);
            await db.SaveChangesAsync();
            return RedirectToAction("Goods");
        }

        [HttpGet]
        [ActionName("Delete")]
        public async Task<IActionResult> ConfirmDelete(int? id)
        {
            if (id != null)
            {
                Book book = await db.Book.FirstOrDefaultAsync(p => p.Id == id);
                if (book != null)
                    return View(book);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id != null)
            {
                Book book = await db.Book.FirstOrDefaultAsync(p => p.Id == id);
                if (book != null)
                {
                    db.Book.Remove(book);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Goods");
                }
            }
            return NotFound();
        }

        [HttpGet]
        public async Task<IActionResult> AddToBasket(int bookid)
        {
            Basket basket = await db.Basket.Include(c => c.BasketBooks).FirstOrDefaultAsync(c => c.UserId == _userManager.GetUserId(HttpContext.User));

            Book book = await db.Book.FirstOrDefaultAsync(p => p.Id == bookid);

            BasketBook basketBook = await db.BasketBook.Where(b => b.BookId == bookid && b.BasketId == basket.Id).FirstOrDefaultAsync();

            if (basket != null && book != null)
            {
                if (basketBook == null)
                {
                    var basketbook = new BasketBook
                    {
                        BasketId = basket.Id,
                        BookId = book.Id
                    };
                    db.Add(basketbook);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Goods");
                }
                
            }
            return NotFound();
        }
    }
}
