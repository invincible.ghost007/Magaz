﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Magaz.Models;
using Magaz.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace Magaz.Controllers
{
    public class UsersController : Controller
    {
        private readonly UserManager<User> _userManager;
        ApplicationContext db;

        public UsersController(ApplicationContext context, UserManager<User> userManager)
        {
            this.db = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            IQueryable<User> users = db.Users;
            return View(await users.AsNoTracking().ToListAsync());
        }

        public IActionResult Basket()
        {
                var basket = db.Basket
                .Include(bb => bb.BasketBooks)
                .ThenInclude(b => b.Book)
                .ThenInclude(a => a.Author)
                .FirstOrDefault(i => i.UserId == _userManager.GetUserId(HttpContext.User));

            return View(basket);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int? bookid)
        {
                Basket basket = db.Basket.Include(c=>c.BasketBooks).FirstOrDefault(c => c.UserId == _userManager.GetUserId(HttpContext.User));

                Book book = await db.Book.FirstOrDefaultAsync(p => p.Id == bookid);

                if (book != null && basket!= null)
                {
                    BasketBook basketbook = basket.BasketBooks.FirstOrDefault(sc => sc.BookId == book.Id);
                    basket.BasketBooks.Remove(basketbook);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Basket");

            }
                return NotFound();
        }
    }
}