﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Magaz.Models
{
    public class Basket
    {
        public int Id { get; set; }

        //Користувач корзини
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        //Книги в корзині
        public List<BasketBook> BasketBooks { get; set; }

        public Basket()
        {
            BasketBooks = new List<BasketBook>();
        }

    }
}
