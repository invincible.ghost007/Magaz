﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Magaz.Models
{
    public class BasketBook
    {
        public int BasketId { get; set; }
        public Basket Basket { get; set; }

        public int BookId { get; set; }
        public Book Book { get; set; }
    }
}
