﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Magaz.Models;

namespace Magaz.Models
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public DbSet<Book> Book { set; get; }
        public DbSet<Basket> Basket { set; get; }
        public DbSet<Author> Author { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .Entity<Basket>()
                .HasOne(u => u.User)
                .WithOne(p => p.UserBasket)
                .HasForeignKey<Basket>(p => p.UserId);

            modelBuilder.Entity<BasketBook>()
                .HasKey(b => new { b.BasketId, b.BookId});

            modelBuilder.Entity<BasketBook>()
                .HasOne(b => b.Basket)
                .WithMany(bb => bb.BasketBooks)
                .HasForeignKey(bi => bi.BasketId);

            modelBuilder.Entity<BasketBook>()
                .HasOne(b => b.Book)
                .WithMany(bb => bb.BasketBooks)
                .HasForeignKey(bi => bi.BookId);
        }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Magaz.Models.BasketBook> BasketBook { get; set; }
    }
}
