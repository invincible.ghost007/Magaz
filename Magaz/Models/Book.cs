﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Magaz.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Author Author { get; set; }
        public int Price { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public int BooksCount { get; set; }
        public List<BasketBook> BasketBooks { get; set; }
       

        public Book()
        {
            BasketBooks = new List<BasketBook>();
        }
    }
}
