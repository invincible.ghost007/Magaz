$(document).ready(function () {
    $(".input-group-addon").click(function() {
        var passinput = $("#password-field");
        if (passinput.attr("type") === "password") {
            passinput.attr("type", "text");
        } else {
            passinput.attr("type", "password");
        }
    });
});