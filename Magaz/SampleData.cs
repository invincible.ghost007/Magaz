﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Magaz.Models;
using Microsoft.AspNetCore.Identity;

namespace Magaz
{
    public class SampleData
    {
        public static void Initialize(ApplicationContext context,
            SignInManager<User> signInManager,
            UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager
            )
        {
            if (!context.Author.Any())
            {
                context.Author.AddRange(
                    new Author
                    {
                        Name = "Томсон М.",
                    },
                    new Author
                    {
                        Name = "Річардсон К.",
                    },
                    new Author
                    {
                        Name = "Маркс Ф.",
                    },
                    new Author
                    {
                        Name = "Петров Г.",
                    },
                    new Author
                    {
                        Name = "Горякович Ч.",
                    },
                    new Author
                    {
                        Name = "Петроградський Н.",
                    },
                    new Author
                    {
                        Name = "Томсон Л.",
                    });
                context.SaveChanges();
            }

            if (!context.Book.Any())
            {
                var bookAuthor = context.Author.ToList();
                context.Book.AddRange(
                    new Book
                    {
                        Name = "В The Economist вибачилися за фразу «громадянська війна на Донбасі»",
                        Author = bookAuthor[0],
                        Price = 200,
                        Image = "2F43526F98C22E1E649666A572C7661C.jpg",
                        Description =
                            "The Economist Intelligence Unit, дослідницька організація The Economist Group, вибачилася за фразу «громадянська війна”,",
                        BooksCount = 3,

                    },
                    new Book
                    {
                        Name = "Старший та молодший Буші прокоментували трагедію в Шарлоттсвіллі",
                        Author = bookAuthor[1],
                        Price = 300,
                        Image = "577DCD471FA7D92D5B1850A5CB37F02C.jpg",
                        Description =
                            "Колишні президенти Джордж Буш-старший і Джордж Буш-молодший закликали США «відмовитися від расизму, антисемітизму",
                        BooksCount = 6
                    },
                    new Book
                    {
                        Name = "Старший та молодший Буші прокоментували трагедію в Шарлоттсвіллі 2",
                        Author = bookAuthor[2],
                        Price = 400,
                        Image = "577DCD471FA7D92D5B1850A5CB37F02C.jpg",
                        Description =
                            "Колишні президенти Джордж Буш-старший і Джордж Буш-молодший закликали США «відмовитися від расизму, антисемітизму",
                        BooksCount = 5
                    },
                    new Book
                    {
                        Name = "Старший та молодший Буші прокоментували трагедію в Шарлоттсвіллі 3",
                        Author = bookAuthor[3],
                        Price = 500,
                        Image = "577DCD471FA7D92D5B1850A5CB37F02C.jpg",
                        Description =
                            "Колишні президенти Джордж Буш-старший і Джордж Буш-молодший закликали США «відмовитися від расизму, антисемітизму",
                        BooksCount = 8
                    },
                    new Book
                    {
                        Name = "Старший та молодший Буші прокоментували трагедію в Шарлоттсвіллі 4",
                        Author = bookAuthor[4],
                        Price = 600,
                        Image = "2F43526F98C22E1E649666A572C7661C.jpg",
                        Description =
                            "Колишні президенти Джордж Буш-старший і Джордж Буш-молодший закликали США «відмовитися від расизму, антисемітизму",
                        BooksCount = 1
                    },
                    new Book
                    {
                        Name = "Чому Україна програла від «журналістського розслідування»",
                        Author = bookAuthor[5],
                        Price = 700,
                        Image = "5F18619AC74A43065C98941313647F07.jpg",
                        Description =
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus aliquam sit amet massa sit amet consequat. Mauris metus magna, aliquam quis tellus non, ullamcorper porttitor est. Ut eu pellentesque sem. ",
                        BooksCount = 10

                    },
                    new Book
                    {
                        Name = "Фонд держмайна продав «Західерго» за ціною на 20 % вищою за стартову",
                        Author = bookAuthor[6],
                        Price = 700,
                        Image = "8B434E6DF776872697D72E7BBA33CD9E.jpg",
                        Description =
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus aliquam sit amet massa sit amet consequat. Mauris metus magna, aliquam quis tellus non, ullamcorper porttitor est. Ut eu pellentesque sem. ",
                        BooksCount = 15
                    }
                );
                context.SaveChanges();
            }

            if (!context.Users.Any())
            {
                var userBasket = context.Basket.ToList();

                if (!roleManager.RoleExistsAsync("user").Result)
                {
                    var roleResult = roleManager.CreateAsync(new IdentityRole("user")).Result;
                }
                if (!roleManager.RoleExistsAsync("bloger").Result)
                {
                    var roleResult1 = roleManager.CreateAsync(new IdentityRole("bloger")).Result;
                }
                if (!roleManager.RoleExistsAsync("admin").Result)
                {
                    var roleResult2 = roleManager.CreateAsync(new IdentityRole("admin")).Result;
                }

                var user = new User
                {
                    Email = "user@gmail.com",
                    UserName = "user", //pass user123123
                    Year = 1998
                };
                var user1 = new User
                {
                    Email = "user1@gmail.com",
                    UserName = "user1", //pass user1123123
                    Year = 1994
                };
                var bloger = new User
                {
                    Email = "bloger@gmail.com",
                    UserName = "Pavlo",
                    Year = 1995
                };
                var bloger1 = new User
                {
                    Email = "bloger1@gmail.com",
                    UserName = "bloger1",
                    Year = 1992
                };
                var bloger2 = new User
                {
                    Email = "bloger2@gmail.com",
                    UserName = "bloger2",
                    Year = 1991
                };
                var bloger3 = new User
                {
                    Email = "bloger3@gmail.com",
                    UserName = "bloger3",
                    Year = 1995
                };
                var bloger4 = new User
                {
                    Email = "bloger4@gmail.com",
                    UserName = "bloger4",
                    Year = 1999
                };
                var bloger5 = new User
                {
                    Email = "bloger5@gmail.com",
                    UserName = "bloge5r",
                    Year = 1992
                };
                var bloger6 = new User
                {
                    Email = "bloger6@gmail.com",
                    UserName = "bloger6",
                    Year = 1993
                };
                var admin = new User
                {
                    Email = "admin@gmail.com",
                    UserName = "admin",
                    Year = 1998
                };

                var userRegisterResult = userManager.CreateAsync(user, "user123123").Result;
                if (userRegisterResult.Succeeded)
                {
                    var x = userManager.AddToRoleAsync(user, "user").Result;
                }

                var userRegisterResult1 = userManager.CreateAsync(user1, "user1123123").Result;
                if (userRegisterResult1.Succeeded)
                {
                    var x1 = userManager.AddToRoleAsync(user1, "admin").Result;
                }

                var userRegisterResult2 = userManager.CreateAsync(bloger, "bloger123123").Result;
                if (userRegisterResult1.Succeeded)
                {
                    var x1 = userManager.AddToRoleAsync(user1, "admin").Result;
                }

                var identityResult1 = userManager.CreateAsync(bloger1, "bloger123123").Result;
                if (identityResult1.Succeeded)
                {
                    var x2 = userManager.AddToRoleAsync(bloger1, "bloger").Result;
                }
                var identityResult2 = userManager.CreateAsync(bloger2, "bloger123123").Result;
                if (identityResult2.Succeeded)
                {
                    var x3 = userManager.AddToRoleAsync(bloger2, "bloger").Result;
                }

                var identityResult3 = userManager.CreateAsync(bloger3, "bloger123123").Result;
                if (identityResult3.Succeeded)
                {
                    var x4 = userManager.AddToRoleAsync(bloger3, "bloger").Result;
                }

                var identityResult4 = userManager.CreateAsync(bloger4, "bloger123123").Result;
                if (identityResult4.Succeeded)
                {
                    var x5 = userManager.AddToRoleAsync(bloger4, "admin").Result;
                }

                var identityResult5 = userManager.CreateAsync(bloger5, "bloger123123").Result;
                if (identityResult5.Succeeded)
                {
                    var x6 = userManager.AddToRoleAsync(bloger5, "bloger").Result;
                }
                var identityResult6 = userManager.CreateAsync(bloger6, "bloger123123").Result;
                if (identityResult6.Succeeded)
                {
                    var x7 = userManager.AddToRoleAsync(bloger6, "bloger").Result;
                }

                context.SaveChanges();
            }

            if (!context.Basket.Any())
            {
                context.Basket.AddRange(
                    
                    new Basket
                    {
                        UserId = context.Users.ToList()[0].Id
                    },

                    new Basket
                    {
                        UserId = context.Users.ToList()[1].Id
                    },

                    new Basket
                    {
                        UserId = context.Users.ToList()[2].Id
                    },

                    new Basket
                    {
                        UserId = context.Users.ToList()[3].Id
                    },

                    new Basket
                    {
                        UserId = context.Users.ToList()[4].Id
                    },

                    new Basket
                    {
                        UserId = context.Users.ToList()[5].Id
                    },

                    new Basket
                    {
                        UserId = context.Users.ToList()[6].Id
                    },

                    new Basket
                    {
                        UserId = context.Users.ToList()[7].Id
                    },

                    new Basket
                    {
                        UserId = context.Users.ToList()[8].Id
                    }
                );
                context.SaveChanges();
            }

            if (!context.Book.FirstOrDefault().BasketBooks.Any())
            {
                List<BasketBook> basketBooks = new List<BasketBook>();
                var bookForBasket = context.Book.ToList()[0];
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[0].Id});
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[1].Id});
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[2].Id });
                context.Book.Update(bookForBasket);
                context.SaveChanges();

                bookForBasket = context.Book.ToList()[1];
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[3].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[4].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[5].Id });
                context.Book.Update(bookForBasket);
                context.SaveChanges();

                bookForBasket = context.Book.ToList()[2];
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[6].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[7].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[8].Id });
                context.Book.Update(bookForBasket);
                context.SaveChanges();

                bookForBasket = context.Book.ToList()[3];
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[0].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[1].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[2].Id });
                context.Book.Update(bookForBasket);
                context.SaveChanges();

                bookForBasket = context.Book.ToList()[4];
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[3].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[4].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[5].Id });
                context.Book.Update(bookForBasket);
                context.SaveChanges();

                bookForBasket = context.Book.ToList()[5];
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[6].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[7].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[8].Id });
                context.Book.Update(bookForBasket);
                context.SaveChanges();

                bookForBasket = context.Book.ToList()[6];
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[0].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[1].Id });
                bookForBasket.BasketBooks.Add(new BasketBook() { BasketId = context.Basket.ToList()[2].Id });
                context.Book.Update(bookForBasket);
                context.SaveChanges();
            }
        }
    }
}
