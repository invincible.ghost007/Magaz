﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Magaz.Models;

namespace Magaz.ViewModels
{
    public class BooksViewModel
    {
        public IEnumerable<Book> Books { get; set; }
        public IEnumerable<Author> Authors { get; set; }
    }
}
